﻿using System;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class PartnerLimit
    {
        public Guid PartnerId { get; set; }
        public Guid LimitId { get; set; }
    }
}

