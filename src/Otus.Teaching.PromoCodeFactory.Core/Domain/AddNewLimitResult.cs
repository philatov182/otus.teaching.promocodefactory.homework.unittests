﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain
{
    public class AddNewLimitResult : Result
    {
        public PartnerLimit PartnerLimit { get; set; }
    }
}
