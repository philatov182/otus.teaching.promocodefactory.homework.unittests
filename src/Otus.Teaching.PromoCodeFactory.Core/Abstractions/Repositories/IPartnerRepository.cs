﻿using System;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface IPartnerRepository<T> : IRepository<T> where T : BaseEntity
    {
        Task<Result> AddNewLimitAsync(Guid id, PartnerPromoCodeLimit partnerPromoCodeLimit);
    }
}
