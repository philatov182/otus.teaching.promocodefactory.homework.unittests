﻿using System;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions
{
    public abstract class Result
    {
        public bool Success { get; set; }
        public Exception Exception { get; set; }
    }
}
