﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost
{
    public static class Configuration
    {
        public static IServiceCollection GetServiceCollection(IConfiguration configuration)
        {
            var serviceCollection = new ServiceCollection();

            serviceCollection.AddScoped(typeof(IRepository<>), typeof(EfRepository<>));
            serviceCollection.AddScoped<IPartnerRepository<Partner>, PartnerRepository>();


            serviceCollection.AddScoped<IDbInitializer, EfDbInitializer>();
            serviceCollection.AddDbContext<DataContext>(x =>
            {
                x.UseSqlite("Filename=PromoCodeFactoryDb.sqlite");
                x.UseSnakeCaseNamingConvention();
                x.UseLazyLoadingProxies();
            });

            return serviceCollection;
        }
    }
}
