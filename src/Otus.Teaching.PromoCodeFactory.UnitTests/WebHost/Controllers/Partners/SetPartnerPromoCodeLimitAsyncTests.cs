﻿using System;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories.RepositoryExceptions;
using Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Builders;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests : IClassFixture<TestFixture>
    {
        private IPartnerRepository<Partner> _repository;

        public SetPartnerPromoCodeLimitAsyncTests(TestFixture testFixture)
        {
            var serviceProvider = testFixture.ServiceProvider;

            _repository = serviceProvider.GetService<IPartnerRepository<Partner>>();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotFound_ReturnsNotFound()
        {
            var mockRepo = new Mock<IPartnerRepository<Partner>>();
            mockRepo.Setup(repo => 
                repo.AddNewLimitAsync(It.IsAny<Guid>(), It.IsAny<PartnerPromoCodeLimit>()))
                .ReturnsAsync(() => new AddNewLimitResult
                {
                    Success = false,
                    Exception = new CustomerNotFoundException($"Партнер с Id: {It.IsAny<Guid>()} не найден")
                });
            var controller = new PartnersController(mockRepo.Object);

            var setPartnerPromoCodeLimitRequest = new SetPartnerPromoCodeLimitRequestBuilder()
                .WithCreateEndDate(DateTime.UtcNow.AddDays(7))
                .WithCreateLimit(10)
                .Build();

            var result = await controller.SetPartnerPromoCodeLimitAsync(Guid.NewGuid(), setPartnerPromoCodeLimitRequest);
            
            result.Should().BeAssignableTo<NotFoundObjectResult>();
        }

        [Theory]
        [InlineData("0da65561-cf56-4942-bff2-22f50cf70d43")]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotActive_ReturnsBadRequest(string partnerId)
        {
            var controller = new PartnersController(_repository);
            var setPartnerPromoCodeLimitRequest = GetSetPartnerPromoCodeLimitRequest();

            var result = await controller.SetPartnerPromoCodeLimitAsync(Guid.Parse(partnerId), setPartnerPromoCodeLimitRequest);
           
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        [Theory]
        [InlineData("7d994823-8226-4273-b063-1a95f3cc1df8")]
        [InlineData("894b6e9b-eb5f-406c-aefa-8ccb35d39319")]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsActive_ReturnsPartnerPromoCodeLimitResponse(string partnerId)
        {
            var controller = new PartnersController(_repository);
            var setPartnerPromoCodeLimitRequest = GetSetPartnerPromoCodeLimitRequest();

            var result = await controller.SetPartnerPromoCodeLimitAsync(Guid.Parse(partnerId), setPartnerPromoCodeLimitRequest);
            
            result.Should().BeAssignableTo<CreatedAtActionResult>();
            var createdAtActionResult = result as CreatedAtActionResult;
            createdAtActionResult.ActionName.Should().Be(nameof(controller.GetPartnerLimitAsync));

            createdAtActionResult.Value.Should().BeAssignableTo<PartnerLimit>();
            var partnerLimitDto = createdAtActionResult.Value as PartnerLimit;
            partnerLimitDto.Should().NotBeNull();

            var partnerPromoCodeLimitResult = await controller.GetPartnerLimitAsync(Guid.Parse(partnerId), partnerLimitDto.LimitId);

            partnerPromoCodeLimitResult.Result.Should().BeAssignableTo<OkObjectResult>();
            (partnerPromoCodeLimitResult.Result as OkObjectResult)?.Value.Should().NotBeNull();
        }

        private SetPartnerPromoCodeLimitRequest GetSetPartnerPromoCodeLimitRequest()
        {
            return new SetPartnerPromoCodeLimitRequest
            {
                EndDate = DateTime.UtcNow.AddDays(7),
                Limit = 10
            };
        }
    }
}