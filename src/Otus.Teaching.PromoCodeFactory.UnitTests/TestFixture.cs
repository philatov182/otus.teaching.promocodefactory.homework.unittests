﻿using System;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.UnitTests.WebHost;

namespace Otus.Teaching.PromoCodeFactory.UnitTests
{
    public class TestFixture : IDisposable
    {
        public IServiceProvider ServiceProvider { get; set; }

        public TestFixture()
        {
            var builder = new ConfigurationBuilder();
            var configuration = builder.Build();
            var serviceCollection = Configuration.GetServiceCollection(configuration);
            ServiceProvider = serviceCollection.BuildServiceProvider();

            ServiceProvider.GetService<IDbInitializer>().InitializeDb();
        }

        public void Dispose()
        {}
    }
}