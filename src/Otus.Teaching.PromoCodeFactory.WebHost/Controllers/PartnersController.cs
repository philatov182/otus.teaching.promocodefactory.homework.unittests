﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories.RepositoryExceptions;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Партнеры
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PartnersController
        : ControllerBase
    {
        private readonly IPartnerRepository<Partner> _repository;

        public PartnersController(IPartnerRepository<Partner> repository)
        {
            _repository = repository;
        }

        [HttpGet]
        public async Task<ActionResult<List<PartnerResponse>>> GetPartnersAsync()
        {
            var partners = await _repository.GetAllAsync();

            var response = partners.Select(x => new PartnerResponse()
            {
                Id = x.Id,
                Name = x.Name,
                NumberIssuedPromoCodes = x.NumberIssuedPromoCodes,
                IsActive = true,
                PartnerLimits = x.PartnerLimits
                    .Select(y => new PartnerPromoCodeLimitResponse()
                    {
                        Id = y.Id,
                        PartnerId = y.PartnerId,
                        Limit = y.Limit,
                        CreateDate = y.CreateDate.ToString("dd.MM.yyyy hh:mm:ss"),
                        EndDate = y.EndDate.ToString("dd.MM.yyyy hh:mm:ss"),
                        CancelDate = y.CancelDate?.ToString("dd.MM.yyyy hh:mm:ss"),
                    }).ToList()
            });

            return Ok(response);
        }
        
        [HttpGet("{id}/limits/{limitId}")]
        public async Task<ActionResult<PartnerPromoCodeLimitResponse>> GetPartnerLimitAsync(Guid id, Guid limitId)
        {
            if (id == Guid.Empty)
            {
                throw new ArgumentNullException(nameof(id));
            }

            if (limitId == Guid.Empty)
            {
                throw new ArgumentNullException(nameof(limitId));
            }

            var partner = await _repository.GetByIdAsync(id);

            if (partner == null)
            {
                return NotFound($"Партнер с Id: {id} не найден");
            }

            var limit = partner.PartnerLimits
                .FirstOrDefault(x => x.Id == limitId);

            if (limit == null)
            {
                return NotFound($"Лимит с Id: {limitId} не найден");
            }

            var response = new PartnerPromoCodeLimitResponse
            {
                Id = limit.Id,
                PartnerId = limit.PartnerId,
                Limit = limit.Limit,
                CreateDate = limit.CreateDate.ToString("dd.MM.yyyy hh:mm:ss"),
                EndDate = limit.EndDate.ToString("dd.MM.yyyy hh:mm:ss"),
                CancelDate = limit.CancelDate?.ToString("dd.MM.yyyy hh:mm:ss"),
            };
            
            return Ok(response);
        }
        
        [HttpPost("{id}/limits")]
        public async Task<IActionResult> SetPartnerPromoCodeLimitAsync(Guid id, SetPartnerPromoCodeLimitRequest request)
        {
            if (id == Guid.Empty)
            {
                throw new ArgumentNullException(nameof(id));
            }

            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }

            var partnerPromoCodeLimit = new PartnerPromoCodeLimit
            {
                EndDate = request.EndDate,
                Limit = request.Limit
            };

            var result = await _repository.AddNewLimitAsync(id, partnerPromoCodeLimit)!;
            var addNewLimitResult = result as AddNewLimitResult;

            if (addNewLimitResult is {Success: false})
            {
                if (addNewLimitResult.Exception is CustomerNotFoundException)
                {
                    return NotFound(addNewLimitResult.Exception.Message);
                }

                if (addNewLimitResult.Exception is CustomerIsNotActiveException ||
                    addNewLimitResult.Exception is LimitMustBeGreaterThanZeroException)
                {
                    return BadRequest(addNewLimitResult.Exception.Message);

                }
            }

            return CreatedAtAction(nameof(GetPartnerLimitAsync), addNewLimitResult?.PartnerLimit);
        }

        [HttpPost("{id}/canceledLimits")]
        public async Task<IActionResult> CancelPartnerPromoCodeLimitAsync(Guid id)
        {
            if (id == Guid.Empty)
            {
                throw new ArgumentNullException(nameof(id));
            }

            var partner = await _repository.GetByIdAsync(id);

            if (partner == null)
            {
                return NotFound($"Партнер с Id: {id} не найден");
            }
            
            //Если партнер заблокирован, то нужно выдать исключение
            if (!partner.IsActive)
            {
                return BadRequest("Данный партнер не активен");
            }

            //Отключение лимита
            var activeLimit = partner.PartnerLimits.FirstOrDefault(x => 
                !x.CancelDate.HasValue);
            
            if (activeLimit != null)
            {
                activeLimit.CancelDate = DateTime.Now;
            }

            await _repository.UpdateAsync(partner);

            return NoContent();
        }
    }
}