﻿using System;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories.RepositoryExceptions
{
    public class CustomerNotFoundException : Exception
    {
        public CustomerNotFoundException(string s) : base(s)
        { }
    }
}
