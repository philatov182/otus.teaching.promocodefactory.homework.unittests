﻿using System;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories.RepositoryExceptions
{
    public class LimitMustBeGreaterThanZeroException : Exception
    {
        public LimitMustBeGreaterThanZeroException(string s) : base(s)
        { }
    }
}
