﻿using System;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories.RepositoryExceptions
{
    public class CustomerIsNotActiveException : Exception
    {
        public CustomerIsNotActiveException(string s) : base(s)
        { }
    }
}
