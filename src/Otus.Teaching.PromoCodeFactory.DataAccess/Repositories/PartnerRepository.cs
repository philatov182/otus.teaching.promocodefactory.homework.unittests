﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories.RepositoryExceptions;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class PartnerRepository : EfRepository<Partner>, IPartnerRepository<Partner>
    {
        public PartnerRepository(DataContext dataContext) : base(dataContext)
        {}

        public async Task<Result> AddNewLimitAsync(Guid id, PartnerPromoCodeLimit partnerPromoCodeLimit)
        {
            var partner = await GetByIdAsync(id);

            if (partner == null)
            {
                return new AddNewLimitResult
                {
                    Success = false,
                    Exception = new CustomerNotFoundException($"Партнер с Id: {id} не найден")
                };
            }

            //Если партнер заблокирован, то нужно выдать исключение
            if (!partner.IsActive)
            {
                return new AddNewLimitResult
                {
                    Success = false,
                    Exception = new CustomerIsNotActiveException("Данный партнер не активен")
                };
            }

            if (partnerPromoCodeLimit.Limit <= 0)
            {
                return new AddNewLimitResult
                {
                    Success = false,
                    Exception = new LimitMustBeGreaterThanZeroException("Лимит должен быть больше 0")
                };
            }

            //Установка лимита партнеру
            var activeLimit = partner.PartnerLimits.FirstOrDefault(x =>
                !x.CancelDate.HasValue);

            if (activeLimit != null)
            {
                //Если партнеру выставляется лимит, то мы 
                //должны обнулить количество промокодов, которые партнер выдал, если лимит закончился, 
                //то количество не обнуляется
                partner.NumberIssuedPromoCodes = 0;

                //При установке лимита нужно отключить предыдущий лимит
                activeLimit.CancelDate = DateTime.Now;
            }

            var newLimit = new PartnerPromoCodeLimit
            {
                Limit = partnerPromoCodeLimit.Limit,
                Partner = partner,
                PartnerId = partner.Id,
                CreateDate = DateTime.Now,
                EndDate = partnerPromoCodeLimit.EndDate
            };

            partner.PartnerLimits.Add(newLimit);

            await UpdateAsync(partner);

            return new AddNewLimitResult
            {
                Success = true,
                PartnerLimit = new PartnerLimit
                {
                    LimitId = newLimit.Id,
                    PartnerId = partner.Id
                }
            };
        }
    }
    
    
}
